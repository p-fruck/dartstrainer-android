This is an Android wrapper for the DartsTrainer PWA (Progressive Web App). DartsTrainer is an open source application
that helps you improve our darts skills. It helps you counting your current points and provides
a set of finish ways - you can pick the one you like best!
Since DartsTrainer is a PWA, it is available on almost any platform that supports a webbrowser.
That's right, even a Tesla! However, don't train and drive...

Features:
* Supports an unlimited amount of players
* Provides all finish ways to the player, in contrast to most other apps that only promote one way
* Supports fiddle for the middle
* It has multiple colored themes all of which are available in a dark and light variant
* Free as in freedom AND as in free beer ;)

Planned features:
* Multi language support
* Multiple gamemodes such as Cricket, Around the clock, etc.
* Statistics
